###############################################################################
#
# PBI system   : Thorlabs DC3100 series FLIM LED driver
# Location     : lab
#
# Support      : https://jira.esss.lu.se/projects/PBITECH
# Wiki         : https://confluence.esss.lu.se/display/PBITECH
#
###############################################################################

# location of the system (section-subsection)
# lab example
epicsEnvSet("RACKROW",                      "PBILAB")
# acquisition unit logical name
epicsEnvSet("ACQ_UNIT",                     "DCX")
# acquisition device ID or name
epicsEnvSet("ACQ_DEVID",                    "DCX")
# acquisition device model
epicsEnvSet("ACQ_MODEL",                    "DC3100 USB")
# acquisition device serial number
epicsEnvSet("ACQ_SERNO",                    "")
# acquisition device USB VID and PID
epicsEnvSet("ACQ_USBVID",                   "0x1313")
epicsEnvSet("ACQ_USBPID",                   "0x8060")

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
# epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")

# include the main startup file
cd startup
< main.cmd
